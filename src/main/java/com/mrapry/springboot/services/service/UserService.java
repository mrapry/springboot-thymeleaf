package com.mrapry.springboot.services.service;

import com.mrapry.springboot.services.entity.User;
import org.springframework.data.repository.CrudRepository;


/**
 * Created by mrapry on 2/7/17.
 */
public interface UserService extends CrudRepository<User, String>{

    public User findByUser (String user);
    public User findByEmail (String email);
}

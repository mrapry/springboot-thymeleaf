package com.mrapry.springboot.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by mrapry on 2/5/17.
 */
@Controller
public class HomeController {
    //  LOGGER
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String index() {
        return "index";
    }



}
